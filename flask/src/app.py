#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, g, abort, session, escape, Response, render_template, flash, url_for, redirect
from utils import get_sha,  make_json_response, enable_only_in_dev


import os
import json
import requests
import logging


ENV_SECRET_KEY = os.environ["ENV_SECRET_KEY"]
IS_DEV = os.environ['IS_DEV']


app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = ENV_SECRET_KEY
app.config['IS_DEV'] = IS_DEV


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)-8s %(message)s')


@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    try:
        return 'patata'
        # return make_json_response(msg='endpoint /')
    except Exception as e:
        return str(e)


@app.route('/only-dev', methods=['GET'])
@enable_only_in_dev
def only_dev():
    return make_json_response(msg='Susto o muerte')



if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)
